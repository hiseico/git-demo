# Git常用命令

## git生成秘钥

```
ssh-keygen -o
```

## 配置用户信息

````sh
  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"
````

## 拉取项目到本地

```sh
git clone 项目的https地址
```

## 将拉取的项目与远程仓库连接

```sh
git remote add 远程仓库名称 远程仓库的Https地址
```

## 更新代码

```sh
git pull 远程库名 master
```

## 将工作区内容添加到暂存区（git add）

> git add命令是将工作区内容添加到暂存区。git commit 将暂存区内容添加到版本库。
>
> git add -A  
>
>  提交被修改(modified)和被删除(deleted)文件，不包括新文件(new)
>
> git add .  提交新文件(new)和被修改(modified)文件，不包括被删除(deleted)文件

```sh
git add -A  #提交所有变化
git add -u #提交被修改(modified)和被删除(deleted)文件，不包括新文件(new)
git add . #提交新文件(new)、被修改(modified)文件和被删除(deleted)文件
git add --ignore-removal . #提交新文件(new)和被修改(modified)文件，不包括被删除(deleted)文件
```

## 将暂存区内容添加到本地版本库（git commit）

```sh
git commit -m "说明"
git commit -a -m "说明"  #加的-a参数可以将所有已跟踪文件中的执行修改或删除操作的文件都提交到本地仓库，即使它们没有经过git add添加到暂存区。新加的文件（即没有被git系统管理的文件）是不能被提交到本地仓库的。建议一般不要使用-a参数，正常的提交还是使用git add先将要改动的文件添加到暂存区，再用git commit 提交到本地版本库。


```

## 将本地版本库变更上传代码到远程仓库

```sh
git push -u 远程库名 master #如果当前分支与多个主机存在追踪关系，那么这个时候-u选项会指定一个默认主机，这样后面就可以不加任何参数使用git push
```

## 使用origin代替仓库名

```sh
git pull origin master #可使用origin代替当前仓库项目名
git push origin master
```

## 放弃本地修改，重置为服务端版本

```sh
git fetch --all
git reset --hard origin/master
```

## 每次进入都需要输入用户名和密码的解决办法

```sh
#git bash进入你的项目目录,输入以下命令，再操作一遍git pull输入一遍密码，以后就不需要再输密码了
git config --global credential.helper store
```



# 分支

### 查看分支

```sh
git branch -a #列出本地及远程所有分支
git branch -r #列出远程分支
```

###  更新远程主机origin 整理分支

```sh
git remote update origin --prune
```

### 删除分支

```sh
git branch -d <branch_name> #删除一个干净的分支(即相对当前分支而言该分支没有新的提交记录)
git branch -D <branch_name> #强制删除一个分支，该分支有没有合并到当前分支的提交记录
git branch -d -r <branch_name> #删除远程及本地分支
```

### 查看分支详情

```sh
git branch -v
```

### 切换分支

```sh
git checkout <branch_name> #切换到指定分支
git checkout -b <branch_name> #创建并切换到指定分支
```

### 分支恢复

**思路：**对于已经有提交记录的分支删除后，实际上只是删除指针，commit记录还保留，如果想恢复，需要使用`git reflog`查找该分支指向的commitId，然后根据commitId创建新的分支
 `git branch <branch_name> <hash_val>`  #根据指定commit创建新分支

### 重命名分支

```sh
git branch -m <branch_name> newname
```

### 分支合并

```sh
git merge <branch_name> #将指定分支合并到当前分支。如果两个分支没有产生分叉情况，那么会进行快速合并，即fast-forward方式，它并不会产生新的commitId，只是改变了指针的指向，产生分叉合并可能会有冲突情况。
git merge <branch_name> #合并分支时禁用Fast forward模式。我们知道如果使用fast-forward方式进行分支合并，只是简单改变了分支指针，而不会产生新的commit记录。为了保证合并数据的完整性，我们也可以在合并时指定不使用fast-forward方式，使用 --no-ff 选项。这样，在merge时就会生成一个新的commit，从日志上就可以看到分支合并记录了。
```

### 冲突解决

当对分叉分支进行合并时，如果两个分支都对同一文件进行了修改，那么合并时就有可能会产生冲突情况。
 如果两个分支对同一文件的修改是有规律的，比如对不同地方的修改，那么git工具可以实现自动合并，如果无法自动合并，则需要对冲突文件进行手动修改，修改完成后使用`git add`表示冲突已经解决，然后使用`git commit`进行提交。

### 分支暂存

`git stash`  #将工作暂存
 `git stash list`  #列出所有的暂存状态
 从暂存区之中进行恢复，有两种处理方式：
 1.先恢复，而后再删除暂存
 `git stash apply`
 `git stash drop`
 2.恢复的同时也将stash内容删除
 `git stash pop`
 当我们在分支上进行代码开发时，有可能会接到突发需求，而当前的代码尚未完成，所以还不能直接提交。
 为了解决这样的问题，git就提供了分支暂存的机制，可以将开发一半的分支进行保存，在适当的时候进行代码恢复。

**示例：**在pro分支上新建文件，然后添加到暂存区表示尚未完成的任务，对当前分支进行暂存，`git status`显示工作空间是干净的。此时应该切换到master分支上创建新的分支完成突发需求(这里就不做演示了)，然后进行分支恢复。

# GitFlow

